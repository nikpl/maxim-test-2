﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Maxim2.Storage
{
    public interface IStorage
    {
        void Put(string key, string value);
        string Get(string key);
        void Delete(string key);
    }
}
