﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Maxim2.Storage
{
    class InMemoryStorage : IStorage
    {
        private ConcurrentDictionary<string, string> dictionary = new ConcurrentDictionary<string, string>();

        public string Get(string key)
        {
            if (dictionary.TryGetValue(key, out var value))
                return value;
            else
                throw new KeyNotFoundException();
        }

        public void Put(string key, string value)
        {
            dictionary.AddOrUpdate(key, value, (_, __) => value);
        }

        public void Delete(string key)
        {
            dictionary.TryRemove(key, out var _);
        }
    }
}
