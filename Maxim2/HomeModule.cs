﻿using Maxim2.Storage;
using Nancy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Maxim2
{
    public class HomeModule : NancyModule
    {
        private IStorage storage;
        public HomeModule(IStorage storage)
        {
            this.storage = storage;

            Get("/{key}", args =>
            {
                try
                {
                    return storage.Get(args.key);
                }
                catch (KeyNotFoundException)
                {
                    return 204;
                }
            });

            Post("/{key}", async args =>
            {
                var value = await GetValue();
                if (String.IsNullOrWhiteSpace(value))
                    return 400;
                storage.Put(args.key, value);
                return 200;
            });

            Delete("/{key}", args =>
            {
                storage.Delete(args.key);
                return 200;
            });
        }

        private async Task<string> GetValue()
        {
            var bodyReader = new StreamReader(Request.Body);
            var bodyText = await bodyReader.ReadToEndAsync();
            return bodyText;
        }
    }
}
